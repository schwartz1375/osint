import os

def get_key(key_name):
    keyFile = 'api.keys'
    try:
        if os.path.exists(keyFile):
            for line in open(keyFile):
                key, value = line.split('::')[0], line.split('::')[1]
                if key == key_name:
                    return value.strip()
        else:
            print('api.keys file is missing, aborting search...\n')
            exit(1)
    except Exception as e:
        print 'AW SNAP!  %s:' % e
        exit(1)