__author__ = 'Matthew Schwartz (@schwartz1375)'
__email__ = ''
__version__ = ''

import sys
import json
import requests
from termcolor import colored
from coreUtils import *

class Censys:
    def __init__(self):
        self.API_URL = 'https://www.censys.io/api/v1'
        self.UID = get_key('UID')
        self.SECRET = get_key('SECRET')

    def searchWeb(self, target, public):
        pages = float('inf')
        page = 1
        print(colored('\nSearching for ' + public + '...', 'magenta'))

        while page <= pages:
            params = {'query': target}
            headers = {'content-type': 'application/json', 'page': page}
            r = requests.post(self.API_URL + '/search/ipv4', json=params, auth=(self.UID, self.SECRET), headers=headers)
            if r.status_code != 200:
                print 'Return code: ' + str(r.status_code)
                print 'Error occurred: %s' % r.json()['error']
                sys.exit(1)
            payload = r.json()
            pages = payload['metadata']['pages']
            countTotal = payload['metadata']['count']
            print(colored('***************************************************', 'blue'))
            print(colored('PAGE ' + str(page), 'blue'))
            print(colored('Total pages returned: ' + str(pages) + ' Total Count: ' + str(countTotal), 'blue'))
            print(colored('***************************************************', 'blue'))
            # print payload #raw json
            # print json.dumps(payload,indent=4) #pretty print JSON
            for r in payload['results']:
                ip = r['ip']
                proto = r['protocols']

                print '[%s] IP: %s - Ports/Protocols: %s' % (colored('*', 'red'), ip, proto)
                # print '[%s] IP: %s' % (colored('*', 'red'), ip)
            page += 1

    def searchCert(self, target, public):
        pages = float('inf')
        page = 1
        print(colored('\nSearching for ' + public + '...', 'magenta'))

        while page <= pages:
            params = {'query': target}
            headers = {'content-type': 'application/json', 'page': page}
            r = requests.post(self.API_URL + '/search/certificates', json=params, auth=(self.UID, self.SECRET), headers=headers)
            if r.status_code != 200:
                print 'Return code: ' + str(r.status_code)
                print 'Error occurred: %s' % r.json()['error']
                sys.exit(1)
            payload = r.json()
            pages = payload['metadata']['pages']
            countTotal = payload['metadata']['count']
            print(colored('***************************************************', 'blue'))
            print(colored('PAGE ' + str(page), 'blue'))
            print(colored('Total pages returned: ' + str(pages) + ' Total Count: ' + str(countTotal), 'blue'))
            print(colored('***************************************************', 'blue'))
            # print payload #raw json
            # print json.dumps(payload, indent=4) #pretty print JSON
            for r in payload['results']:
                subjectDN = r['parsed.subject_dn']
                issuedCommonName = r['parsed.issuer_dn']
                print '[%s] Subject DN: %s - Issuer Common Name: %s' % (colored('*', 'red'), subjectDN, issuedCommonName)
            page += 1

    def getRepos(self):
        print(colored('\nRetrieve the list of series...', 'magenta'))
        res = requests.get(self.API_URL + '/data', auth=(self.UID, self.SECRET))
        if res.status_code != 200:
            print 'Return code: ' + str(res.status_code)
            print 'Error occurred: %s' % res.json()['error']
        for name, series in res.json()['raw_series'].iteritems():
            print series['name'], 'was last updated at', series['latest_result']['timestamp']


def main():
    try:
        censys = Censys()
        censys.getRepos()
        t1 = '80.http.get.headers.server: Apache'
        t1public = 'Apache Web Serbers'
        censys.searchWeb(t1, t1public)
        tc1 = 'not 443.https.tls.validation.browser_trusted:true'
        tc1public = 'Browser untrusted certs'
        censys.searchCert(tc1, tc1public)
        return 0
    except (KeyboardInterrupt, SystemExit):
        print("Aborted by user (ctrl-c pressed)...")
        return 130
    except SystemExit as e:
        print('System Exited: %s' % e)
        return 1
    except Exception as e:
        print('General Error: %s' % e)
        return 1


if __name__ == '__main__':
    sys.exit(main())
