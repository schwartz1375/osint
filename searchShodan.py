__author__ = 'Matthew Schwartz (@schwartz1375)'
__email__ = ''
__version__ = ''


import sys
# import json
from coreUtils import *
import shodan
from termcolor import colored


class Shodan:
    def searchShodan(self, target, public):
        print(colored('\nStarting Shodan Search for ' + public + '...', 'magenta'))

        SHODAN_API_KEY = get_key('SHODAN')
        api = shodan.Shodan(SHODAN_API_KEY)
        try:
            # Search Shodan
            results = api.search(target)

            # Show the results
            print(colored('***************************************************', 'blue'))
            print(colored('Total number of results found: %s' % results['total'], 'blue'))
            print(colored('***************************************************', 'blue'))
            # print json.dumps(results, indent=4) #pretty print JSON

            # Loop through the matches and print each IP
            for service in results['matches']:
                # print '[%s] IP: %s - Protocol: %s on port: %s' % (colored('*', 'red'), service['ip_str'], service['_shodan']['module'], str(service['port']))
                results = api.search('ip:' + service['ip_str'])
                # We need a nested look to lookup all the services for a host
                for data in results['matches']:
                    print '[%s] IP: %s - Protocol: %s on port: %s' % (
                        colored('*', 'red'), data['ip_str'], data['_shodan']['module'], str(data['port']))

        except Exception as e:
            print 'General Error: %s' % e


def main():
    try:
        shodan = Shodan()
        shodan.searchShodan('Apache', 'Web Servers')
        return 0
    except (KeyboardInterrupt, SystemExit):
        print("Aborted by user (ctrl-c pressed)...")
        return 130
    except SystemExit as e:
        print('System Exited: %s' % e)
        return 1
    except Exception as e:
        print('General Error: %s' % e)
        return 1


if __name__ == '__main__':
    sys.exit(main())
